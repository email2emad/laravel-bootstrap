<!-- Modal -->
<div class="modal fade" id="item-create-log" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New Transaction Log Entry</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('transactions.store')}}" method="POST" autocomplete="off">
            @csrf
            @method('POST')
            {{-- <form action="" method="GET" autocomplete="off"> --}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Title</label>
                        <input type="text" name="title" placeholder="Log Title" class="form-control">
                    </div>                    
                    <div class="form-group">
                        <label for="name">Comment (Optional)</label>
                        <textarea type="text" name="comment" placeholder="Comment" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="count">Quantity</label>
                        <input type="number" name="count" placeholder="Quantity" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="type">Type of Transaction</label>
                        <select name="type" class="custom-select">
                            <option value="in">Purchase</option>
                            <option value="out">Sale</option>   
                            <option value="out">Discard</option>  
                            <option value="out">Damaged</option>                               
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="number" name="item_id" value="{{$item->id}}" class="form-control" hidden>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>