<!-- Modal -->
<div class="modal fade" id="item-edit-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('items.update', $item)}}" method="POST" autocomplete="off">
            @csrf
            @method('PUT')
                <div class="modal-body">   
                    <div class="form-group">
                        <label for="name">Item Name</label>
                        <input type="text" name="name" value="{{$item->name}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Description</label>
                        <textarea type="text" name="description" class="form-control">{{$item->description}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="name">Category</label>
                        <select name="category_id" class="custom-select">
                                <option value="{{$item->category->id}}" selected>{{$item->category->name}}</option>
                            @foreach ($categories as $category)
                                @if ($category->id !== $item->category->id)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endif                                
                            @endforeach                            
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>