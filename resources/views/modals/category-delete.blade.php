<!-- Modal -->
<div class="modal fade" id="category-delete-{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Category</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>            
            <div class="modal-body">   
                Are you sure you wish to delete <b class="text-danger">{{$category->name}}</b> category?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <form action="{{route('categories.destroy', $category)}}" method="POST" autocomplete="off">
                    @csrf
                    @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>           
        </div>
    </div>
</div>