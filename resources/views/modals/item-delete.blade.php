<!-- Modal -->
<div class="modal fade" id="item-delete-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Item</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>            
            <div class="modal-body">   
                Are you sure you wish to delete <span class="text-danger"><b>{{$item->name}}</b> - {{$item->description}}</span>?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <form action="{{route('items.destroy', $item)}}" method="POST" autocomplete="off">
                    @csrf
                    @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>           
        </div>
    </div>
</div>