<!-- Modal -->
<div class="modal fade" id="item-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('items.store')}}" method="POST" autocomplete="off">
            @csrf
            @method('POST')
            {{-- <form action="" method="GET" autocomplete="off"> --}}
                <div class="modal-body">   
                    <div class="form-group">
                        <label for="name">Item Name</label>
                        <input type="text" name="name" placeholder="Item Name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Description</label>
                        <textarea type="text" name="description" placeholder="Description" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="name">Category</label>
                        <select name="category_id" class="custom-select">
                            <option value="">-- Select Category --</option>
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach                            
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>