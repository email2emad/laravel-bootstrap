@extends('layouts.app')

@section('content')
    <div class="card mb-3">
        <div class="card-header bg-dark text-white d-flex justify-content-between align-items-center">
            <h3 class="card-title">{{$item->category->name}} <i class="fa fa-caret-right"></i> {{$item->name}}</h3>
            <!-- Button trigger modal -->
            <div>
                <a class="btn btn-sm btn-outline-light" href="#" data-toggle="modal" data-target="#item-edit-{{$item->id}}"><i class="fa fa-edit"></i></a>
                <a class="btn btn-sm btn-outline-light" href="#" data-toggle="modal" data-target="#item-delete-{{$item->id}}"><i class="fa fa-trash"></i></a>
            </div>
        </div> 
        <div class="card-body">
           <p><b>Descritption: </b>  {{$item->description}}</p>
           <p><b>Category:</b> {{$item->category->name}}</p>
           <p><b>In Stock: </b>{{$item->transactions->where('type','in')->sum('count') - $item->transactions->where('type','out')->sum('count')}}</p>               
        </div>
    </div>
    <div class="card">
        <div class="card-header bg-dark text-white d-flex justify-content-between align-items-center">
            <h3 class="card-title">Track Record</h3>
            <!-- Button trigger modal -->
            <div>
                <a class="btn btn-sm btn-outline-light" href="#" data-toggle="modal" data-target="#item-create-log"><i class="fa fa-plus"></i></a>
            </div>
        </div> 
        <div class="card-body">
           <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Timestamp</th>
                            <th>Details</th>
                            <th>In/Out</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($item->transactions ?? '' as $transaction)
                            <tr @if ($transaction->type == 'in')
                                class="text-success"                                
                            @else
                                class="text-danger"
                            @endif>
                                <td>{{$transaction->id}}</td>
                                <td>{{$transaction->created_at}}</td>
                                <td>{{$transaction->title}} {{$transaction->comment ?? ''}}</td>
                                <td>{{$transaction->type}}</td>
                                <td>{{$transaction->count}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
           </div>              
        </div>
    </div>
    @include('modals.item-edit')
    @include('modals.item-delete')
    @include('modals.item-create-log')
@endsection