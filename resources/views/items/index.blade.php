@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header bg-dark text-white d-flex justify-content-between align-items-center">
            <h3 class="card-title">Inventory</h3>
            {{-- Search button --}}
            <form action="?" method="GET" autocomplete="off">
            <div class="input-group mx-5">
                <input type="text" class="form-control input-sm" placeholder="Search for Item" name="search">
                <div class="input-group-append">
                  <button class="btn btn-sm btn-outline-light px-3" type="submit"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#item-create">
                <i class="fa fa-plus"></i>
            </button>
        </div> 
        <div class="card-body">
            <div class="mb-3">
                <a href="{{route('items.index')}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-filter"></i> All</a>
                @foreach ($categories as $category)
                    <a href="{{route('items.index')}}?category={{$category->id}}" class="btn btn-sm btn-outline-primary"><i class="fa fa-filter"></i> {{$category->name}}</a>
                @endforeach
            </div>
            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Quantity</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($items->count() <= 0)
                    <tr class="text-center"><td colspan="5">No items to show <a data-toggle="modal" data-target="#item-create" href="#">Add New?</a></td></tr>
                        @else
                        @foreach ($items as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td><a href="{{route('items.show', $item)}}">{{$item->name}}</a></td>
                                <td>{{$item->description}}</td>
                                <td>{{$item->category->name}}</td>
                                <td>{{$item->transactions->where('type','in')->sum('count') - $item->transactions->where('type','out')->sum('count')}}</td>
                                <td>
                                    <a class="badge badge-primary" href="#" data-toggle="modal" data-target="#item-edit-{{$item->id}}"><i class="fa fa-edit"></i></a>
                                    <a class="badge badge-danger" href="#" data-toggle="modal" data-target="#item-delete-{{$item->id}}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @include('modals.item-edit')
                            @include('modals.item-delete')
                        @endforeach                        
                        @endif
                    </tbody>
                </table>
            </div>            
        </div>
    </div>
    @include('modals.item-create')
@endsection