@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header bg-dark text-white d-flex justify-content-between align-items-center">
            <h3 class="card-title">Inventory Log</h3>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#log-create">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                        <tr>
                            <th>#</th>                            
                            <th>Title</th>
                            <th>Item</th>
                            <th>In/Out</th>
                            <th>Quantity</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Add Row</td>
                        <form action="{{route('transactions.store')}}" method="POST" autocomplete="off">
                            @csrf
                            @method('POST')
                            <td><input type="text" name="title" class="form-control" placeholder="Title"></td>
                            <td>
                                <input list="items-list" name="item_id" id="item_id" class="form-control"  placeholder="Item Code">
                                <datalist id="items-list">
                                    @foreach ($items as $item)
                                        <option value="{{$item->id}}" label="{{$item->name}}">
                                    @endforeach
                                </datalist>
                            </td>
                            <td>
                                <select name="type" class="custom-select">
                                    <option value="in">Purchase</option>
                                    <option value="out">Sale</option>   
                                    <option value="out">Discard</option>  
                                    <option value="out">Damaged</option>                               
                                </select>
                            </td>
                            <td><input type="text" name="count" class="form-control" placeholder="Quantity"></td>
                            <td><button type="submit" class="btn btn-block btn-primary"><i class="fa fa-save"></i> Save</button></td>
                        </form>
                        </tr>
                        @foreach ($transactions as $transaction)

                            <tr 
                            @if ($transaction->type == 'in')
                                class="text-success"                                
                            @else
                                class="text-danger"
                            @endif
                            >
                                <td>{{$transaction->id}}</td>                                
                                <td>{{$transaction->title}}</td>
                                <td>{{$transaction->item->name}}</td>
                                <td>{{$transaction->type}}</td>
                                <td>{{$transaction->count}}</td>
                                <td>{{$transaction->created_at}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection