@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header bg-dark text-white d-flex justify-content-between align-items-center">
            <h3 class="card-title">Categories List</h3>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-outline-light" data-toggle="modal" data-target="#category-create">
                <i class="fa fa-plus"></i>
            </button>
        </div> 
        <div class="card-body">
            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Items</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($categories) <= 0)
                        <tr class="text-center"><td colspan="4">Categories list empty. <a href="#" data-toggle="modal" data-target="#category-create">Add New?</a></td></tr>
                        @else
                            @foreach ($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td><a href="{{route('items.index')}}?category={{$category->id}}">{{ $category->name }}</a></td>
                                <td>{{ $category->items()->count() }}</td>
                                <td>
                                    <a class="badge badge-primary" href="#" data-toggle="modal" data-target="#category-edit-{{$category->id}}"><i class="fa fa-edit"></i></a>
                                    <a class="badge badge-danger" href="#" data-toggle="modal" data-target="#category-delete-{{$category->id}}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                                @include('modals.category-edit')
                                @include('modals.category-delete')
                            @endforeach               
                        @endif
                    </tbody>
                </table>  
            </div>
        </div>
    </div>
    @include('modals.category-create')
@endsection