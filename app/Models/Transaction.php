<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'comment', 'type', 'count', 'item_id'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
